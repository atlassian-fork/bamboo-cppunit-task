package com.atlassian.bamboo.plugins.cppunit;

import com.atlassian.bamboo.configuration.*;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultError;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Sets;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

public class CppUnitParser extends DefaultContentHandler
{
    private static final ElementParser DO_NOTHING_PARSER = new DefaultElementParser();

    private static final String FAILURE_TYPE_ERROR = "Error";
    private static final String FAILURE_TYPE_ASSERTION = "Assertion";
    public static final String NAMESPACE_SEP = "::";

    private String testName;
    private String testMessage;
    private boolean isFailed = false;
    private boolean isError = false;

    private Set<TestResults> failedTests;
    private Set<TestResults> passedTests;

    public CppUnitParser()
    {
        registerElementParser("FailedTest", new FailedTestParser());
        registerElementParser("Test", new TestParser());
        registerElementParser("Name", new NameParser());
        registerElementParser("FailureType", new FailureTypeParser());
        registerElementParser("Message", new MessageParser());
    }

    public Set<TestResults> getFailedTests()
    {
        return failedTests;
    }

    public Set<TestResults> getPassedTests()
    {
        return passedTests;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        if (!hasParserFor(localName))
        {
            registerElementParser(localName, DO_NOTHING_PARSER);
        }
        super.startElement(uri, localName, qName, attributes);
    }

    public void parse(InputStream inputStream) throws IOException, SAXException
    {
        failedTests = Sets.newLinkedHashSet();
        passedTests = Sets.newLinkedHashSet();

        reset();

        XMLReader reader = XMLReaderFactory.createXMLReader(DEFAULT_PARSER);

        reader.setContentHandler(this);
        reader.parse(new InputSource(inputStream));
    }

    class FailedTestParser extends DefaultElementParser
    {
        @Override
        public void startElement(Attributes attributes)
        {
            isFailed = true;
        }

        @Override
        public void endElement() throws ConfigurationException
        {
            TestResults result = createResult(testName);

            if (isError)
            {
                result.setState(TestState.FAILED);
                TestCaseResultError error = new TestCaseResultErrorImpl(testMessage);
                result.addError(error);
            }
            else
            {
                result.setSystemOut(testMessage);
            }

            failedTests.add(result);

            reset();
        }
    }

    class TestParser extends DefaultElementParser
    {
        @Override
        public void endElement() throws ConfigurationException
        {
            TestResults result = createResult(testName);
            result.setState(TestState.SUCCESS);
            passedTests.add(result);

            reset();
        }
    }

    class NameParser extends ElementContentElementParser
    {
        @Override
        public void endElement() throws ConfigurationException
        {
            testName = getElementContent();
        }
    }

    class FailureTypeParser extends ElementContentElementParser
    {
        @Override
        public void endElement() throws ConfigurationException
        {
            if (FAILURE_TYPE_ERROR.equalsIgnoreCase(getElementContent()) ||
                FAILURE_TYPE_ASSERTION.equalsIgnoreCase(getElementContent()))
            {
                isError = true;
            }
        }
    }

    class MessageParser extends ElementContentElementParser
    {
        @Override
        public void endElement() throws ConfigurationException
        {
            testMessage = getElementContent();
        }
    }

    private void reset()
    {
        isFailed = false;
        isError = false;
        testMessage = null;
        testName = null;
    }

    private TestResults createResult(String testName)
    {
        String className = "";
        String methodName = testName;

        if (StringUtils.contains(testName, NAMESPACE_SEP))
        {
            String[] parts = StringUtils.split(testName, NAMESPACE_SEP);
            if (parts.length > 0)
            {
                methodName = parts[parts.length-1];
                parts = (String[])ArrayUtils.remove(parts, parts.length -1);
                className = StringUtils.join(parts, NAMESPACE_SEP);
            }
        }

        return new TestResults(className, methodName, "0");
    }
}
